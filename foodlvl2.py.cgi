#!/usr/bin/python

import sys
import re
import cgi

form = cgi.FieldStorage()
location = form.getvalue("location")
foodtype = form.getvalue("type")
#stores all the stores which sell coffee

a = []

if location:
    if location == "Lower":
        f = open('info/LowerCampus.txt', 'r')
    elif location == "Middle":
        f = open('info/MiddleCampus.txt', 'r')
    elif location == "Upper":
        f = open('info/UpperCampus.txt', 'r')
    else:
        print "not working"
        sys.exit()
     
    for line in f:
        if re.search('coffee', line):
            a.append(line)
           
           #just stuff
elif foodtype:
    
    if foodtype == "meals":
        f = open('info/LowerCampus.txt', 'r')
        for line in f:
            if re.search('meals', line):
                a.append(line)
        
        f = open('info/MiddleCampus.txt', 'r')
        for line in f:
            if re.search('meals', line):
                a.append(line)
        
        f = open('info/UpperCampus.txt', 'r')
        for line in f:
            if re.search('meals', line):
                a.append(line)
        
    elif foodtype == "sweets":
        f = open('info/LowerCampus.txt', 'r')
        for line in f:
            if re.search('sweets', line):
                a.append(line)
        
        f = open('info/MiddleCampus.txt', 'r')
        for line in f:
            if re.search('sweets', line):
                a.append(line)
        
        f = open('info/UpperCampus.txt', 'r')
        for line in f:
            if re.search('sweets', line):
                a.append(line)
       
    elif foodtype == "snacks":
        f = open('info/LowerCampus.txt', 'r')
        for line in f:
            if re.search('snacks', line):
                a.append(line)
        
        f = open('info/MiddleCampus.txt', 'r')
        for line in f:
            if re.search('snacks', line):
                a.append(line)
        
        f = open('info/UpperCampus.txt', 'r')
        for line in f:
            if re.search('snacks', line):
                a.append(line)
        
    else:
        print "not working"
        sys.exit()


print "Content-type: text/html; charset=utf-8\n\n"
f2 = open('pageStart.txt', 'r')
for line in f2:
    print line

print "<div class=\"row\" align=\"center\">"
for string in a:
    print "<div class=\"col-md-4 portfolio-item info-box\">"
    temp = re.split(';', string)
    for i in range(0,len(temp)-1):
        if i != 2:
            print temp[i]+"    ";
    print "</div>"
print "</div>"

f2 = open('pageEnd.txt', 'r')
for line in f2:
    print line
